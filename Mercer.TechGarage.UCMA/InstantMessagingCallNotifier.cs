﻿/********************************************************
*                                                       *
*   Copyright (C) Microsoft. All rights reserved.       *
*                                                       *
********************************************************/

// .NET namespaces
using System;
using System.Threading;

// UCMA namespaces
using Microsoft.Rtc.Collaboration;
using Microsoft.Rtc.Signaling;

// UCMA samples namespaces
using Microsoft.Rtc.Collaboration.Sample.Common;
using System.Text;
using Mercer.TechGarage.UCMA.MessageHandlers;
using Mercer.TechGarage.UCMA;
using BuildABot.Util;
using System.Collections.Generic;
using Mercer.TechGarage.UCMA.Feedback;

namespace Microsoft.Rtc.Collaboration.Sample.BasicInstantMessagingCall
{
    public class InstantMessagingCallNotifier
    {
        #region Locals
        // The information for the conversation and the far end participant.        
        
        // Subject of the conversation; will appear in the center of the title bar of the 
        // conversation window if Microsoft Lync is the far end client.
        private static String _conversationSubject = "New Calendar Event"; 
        
        // Priority of the conversation will appear in the left corner of the title bar of the
        // conversation window if Microsoft Lync is the far end client.
        private static String _conversationPriority = ConversationPriority.Normal;

        // The Instant Message that will be sent to the far end.
        private static String _messageToSend = "Hello World! I am a bot, and will echo whatever you type. " + 
            "Please send 'bye' to end this application.";
        /// <summary>
        /// List of bots maintained by the UC host. Every user (actually every messaging flow) gets it's own bot.
        /// </summary>
        private Dictionary<InstantMessagingFlow, Bot> bots = new Dictionary<InstantMessagingFlow, Bot>();

        /// <summary>
        /// Gets or sets the feedback engine.
        /// </summary>
        /// <value>The feedback engine.</value>
        public FeedbackEngine FeedbackEngine { get; set; }

        /// <summary>
        /// Occurs whenever the UCHost receives a message.
        /// </summary>
        public event MessageEventHandler MessageReceived;
        private Dictionary<InstantMessagingFlow, InstantMessagingCall> messageCalls = new Dictionary<InstantMessagingFlow, InstantMessagingCall>();
       // private InstantMessagingCall _instantMessagingCall;
        
       // private InstantMessagingFlow _instantMessagingFlow;

        /// <summary>
        /// Occurs when UC bot replied to the message.
        /// </summary>
        public event ReplyEventHandler Replied;
        //private ApplicationEndpoint _userEndpoint;
        //private UserEndpoint _userEndpoint;
        /// <summary>
        /// In case Bot didn't understand what user said, it doesn't make sense to show same help message over and over again when users are part of the conference because most likely they are chatting to themselves.
        /// Thus, this collection will help with understanding when Bot need to show help message in case he didn't understand the user.
        /// </summary>
        private Dictionary<InstantMessagingFlow, bool> conversationFlowMisunderstandingAlerts = new Dictionary<InstantMessagingFlow, bool>();

        //private UCMASampleHelper _helper;
        /// <summary>
        /// Helptext is displayed if bot does not understand user's message.
        /// </summary>
        private string helpText = string.Format("Sorry, I didn't get you. Type {0}  to see what I can understand by default.", "help".EncloseRtfBold()).EncloseRtf();

        // Event to notify application main thread on completion of the sample.
        private AutoResetEvent _sampleCompletedEvent = new AutoResetEvent(false);
        #endregion

        #region Methods
        /// <summary>
        /// Instantiate and run the InstantMessagingCall quickstart.
        /// </summary>
        /// <param name="args">unused</param>
        /*public static void Main(string[] args)
        {
            UCMASampleInstantMessagingCall ucmaSampleInstantMessagingCall = 
                                                                        new UCMASampleInstantMessagingCall();
            ucmaSampleInstantMessagingCall.Run();
        }*/

       
        public void Run(UserEndpoint _userEndpoint, string calledParty, string MessageTitle, string Message)
        {

            // Initialize and startup the platform.
            _messageToSend = Message;
            Exception ex = null;
            try
            {

                // Create the UserEndpoint

                //_helper = new UCMASampleHelper();
                //_userEndpoint = _helper.CreateEstablishedUserEndpoint("BOT Messaging"); /*endpointFriendlyName*/

               // if (_helper == null)
               // {
               //     return;
               // }


                //_userEndpoint = _helper.CreateAutoProvisionedApplicationEndpoint();

                Console.Write("The User Endpoint owned by URI: ");
                Console.Write(_userEndpoint.OwnerUri);
                Console.WriteLine(" is now established and registered.");

                // Setup the conversation and place the call.
                ConversationSettings convSettings = new ConversationSettings();
                convSettings.Priority = _conversationPriority;
                convSettings.Subject = (!String.IsNullOrEmpty(MessageTitle))? MessageTitle : _conversationSubject;

                // Conversation represents a collection of modes of communication
                // (media types)in the context of a dialog with one or multiple 
                // callees.
                Conversation conversation = new Conversation(_userEndpoint, convSettings);
                var instantMessagingCall = new InstantMessagingCall(conversation);

                // Call: StateChanged: Only hooked up for logging. Generally, 
                // this can be used to surface changes in Call state to the UI
                instantMessagingCall.StateChanged += this.InstantMessagingCall_StateChanged;

                // Subscribe for the flow created event; the flow will be used to
                // send the media (here, IM).
                // Ultimately, as a part of the callback, the messages will be 
                // sent/received.
                instantMessagingCall.InstantMessagingFlowConfigurationRequested += 
                    this.InstantMessagingCall_FlowConfigurationRequested;

                // Get the sip address of the far end user to communicate with.
                /*String _calledParty = "sip:" + 
                    UCMASampleHelper.PromptUser(
                    "Enter the URI of the user logged onto Microsoft Lync, in the User@Host format => ", 
                    "RemoteUserURI");
                */
                String _calledParty = calledParty; //"sip:arun.agrawal@ocs.mmc.com";
                
                // Place the call to the remote party, without specifying any 
                // custom options. Please note that the conversation subject 
                // overrides the toast message, so if you want to see the toast 
                // message, please set the conversation subject to null.
                instantMessagingCall.BeginEstablish(_calledParty, new ToastMessage("Hello Toast"), null, 
                    CallEstablishCompleted, instantMessagingCall);

                
            }
            catch (InvalidOperationException iOpEx)
            {
                // Invalid Operation Exception may be thrown if the data provided
                // to the BeginXXX methods was invalid/malformed.
                // TODO (Left to the reader): Write actual handling code here.
                ex = iOpEx;
            }
            finally
            {
                if (ex != null)
                {
                    // If the action threw an exception, terminate the sample, 
                    // and print the exception to the console.
                    // TODO (Left to the reader): Write actual handling code here.
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine("Shutting down platform due to error: " + ex.Message);
                   // _helper.ShutdownPlatform();
                }
            }

            // Wait for sample to complete
            _sampleCompletedEvent.WaitOne();

        }
              

        // Just to record the state transitions in the console.
        void InstantMessagingCall_StateChanged(object sender, CallStateChangedEventArgs e)
        {
            Console.WriteLine("Call has changed state. The previous call state was: " + e.PreviousState + 
                "and the current state is: " + e.State);
        }

        // Flow created indicates that there is a flow present to begin media 
        // operations with, and that it is no longer null.
        public void InstantMessagingCall_FlowConfigurationRequested(object sender, 
            InstantMessagingFlowConfigurationRequestedEventArgs e)
        {
            Console.WriteLine("Flow Created.");
            var instantMessagingFlow = e.Flow;


                        
            // Now that the flow is non-null, bind the event handlers for State 
            // Changed and Message Received. When the flow goes active, 
            // (as indicated by the state changed event) the program will send 
            // the IM in the event handler.
            instantMessagingFlow.StateChanged += this.InstantMessagingFlow_StateChanged;

            // Message Received is the event used to indicate that a message has
            // been received from the far end.
            instantMessagingFlow.MessageReceived += this.InstantMessagingFlow_MessageReceived;

            // Also, here is a good place to bind to the 
            // InstantMessagingFlow.RemoteComposingStateChanged event to receive
            // typing notifications of the far end user.
            //_instantMessagingFlow.RemoteComposingStateChanged += 
            //                                        this.InstantMessagingFlow_RemoteComposingStateChanged;
        }

        private void InstantMessagingFlow_StateChanged(object sender, MediaFlowStateChangedEventArgs e)
        {
            InstantMessagingFlow instantMessagingFlow = (InstantMessagingFlow)sender;

            Console.WriteLine("Flow state changed from " + e.PreviousState + " to " + e.State);

            // When flow is active, media operations (here, sending an IM) 
            // may begin.
            if (e.State == MediaFlowState.Active)
            {
                // Send the message on the InstantMessagingFlow.
                byte[] htmlBytes = Encoding.UTF8.GetBytes(_messageToSend);
                instantMessagingFlow.BeginSendInstantMessage(new System.Net.Mime.ContentType("text/html"), htmlBytes, SendMessageCompleted, 
                    instantMessagingFlow);
                
            }
        }

        private void InstantMessagingFlow_RemoteComposingStateChanged(object sender, 
                                                                        ComposingStateChangedEventArgs e)
        {
            // Prints the typing notifications of the far end user.
            Console.WriteLine("Participant " 
                                + e.Participant.Uri.ToString() 
                                + " is " 
                                + e.ComposingState.ToString()
                                );
        }


        private void InstantMessagingFlow_MessageReceived(object sender, InstantMessageReceivedEventArgs e)
        {

            InstantMessagingFlow instantMessagingFlow = (InstantMessagingFlow)sender;

            string conferenceUri = string.Empty;
            string conversationId = string.Empty;
            if (this.messageCalls.ContainsKey(instantMessagingFlow))
            {
                conferenceUri = this.messageCalls[instantMessagingFlow].Conversation.ConferenceSession.ConferenceUri;
                conversationId = this.messageCalls[instantMessagingFlow].Conversation.Id;
            }
            Message message = new Message(e.TextBody, e.Sender.DisplayName, StringHelper.GetAliasFromSip(e.Sender.UserAtHost), DateTime.Now, conversationId, conferenceUri);
            if (this.MessageReceived != null)
            {
                this.MessageReceived(this, new MessageEventArgs(message));
            }
            if (!this.bots.ContainsKey(instantMessagingFlow))
            {
                this.RegisterBot(instantMessagingFlow, conferenceUri);
            }
            try
            {
                this.bots[instantMessagingFlow].ProcessMessage(message);
            }

            catch (Exception ex)
            {
                string errorMessage =
                    "Sorry, I'm having trouble to get the requested information because an error happened! "
                    + Emoticons.Sad
                    + Environment.NewLine
                    + "The error was: " + ex.Message;

                //this.RaiseErrorOccured(message.Content, ex);
                //this.SendReply(instantMessagingFlow, new ReplyMessage(errorMessage));
            }
            // On an incoming Instant Message, print the contents to the console.
            //Console.WriteLine(e.Sender.Uri + " said: " + e.TextBody);


            // Shutdown if the far end tells us to.
            /*if (e.TextBody.Equals("bye", StringComparison.OrdinalIgnoreCase))
            {
                // Shutting down the platform will terminate all attached objects.
                // If this was a production application, it would tear down the 
                // Call/Conversation, rather than terminating the entire platform.
                _instantMessagingFlow.BeginSendInstantMessage("Shutting Down...", SendMessageCompleted, 
                    _instantMessagingFlow);
                _helper.ShutdownPlatform();
                _sampleCompletedEvent.Set();
            }
            else
            {
                Message _message = new Message(e.TextBody);
                Bot bot = new Bot();
                bot.ProcessMessage(_message, false);
                // Echo the instant message back to the far end (the sender of 
                // the instant message).
                // Change the composing state of the local end user while sending messages to the far end.
                // A delay is introduced purposely to demonstrate the typing notification displayed by the 
                // far end client; otherwise the notification will not last long enough to notice.
                //_instantMessagingFlow.LocalComposingState = ComposingState.Composing;
                //Thread.Sleep(2000);

                //Echo the message with an "Echo" prefix.
                //_instantMessagingFlow.BeginSendInstantMessage("Echo: " + e.TextBody, SendMessageCompleted, 
                //    _instantMessagingFlow);
            }*/

        }


        private void CallEstablishCompleted(IAsyncResult result)
        {
            InstantMessagingCall instantMessagingCall = result.AsyncState as InstantMessagingCall;
            Exception ex = null;
            try
            {
                instantMessagingCall.EndEstablish(result);
                Console.WriteLine("The call is now in the established state.");

                if (!this.messageCalls.ContainsKey(instantMessagingCall.Flow) && !this.conversationFlowMisunderstandingAlerts.ContainsKey(instantMessagingCall.Flow))
                {
                    this.messageCalls.Add(instantMessagingCall.Flow, instantMessagingCall);
                    this.conversationFlowMisunderstandingAlerts.Add(instantMessagingCall.Flow, false);
                }


            }
            catch (OperationFailureException opFailEx)
            {
                // OperationFailureException: Indicates failure to connect the 
                // call to the remote party.
                // TODO (Left to the reader): Write real error handling code.
                ex = opFailEx;
            }
            catch (RealTimeException rte)
            {
                // Other errors may cause other RealTimeExceptions to be thrown.
                // TODO (Left to the reader): Write real error handling code.
                ex = rte;
            }
            finally
            {
                if (ex != null)
                {
                    // If the action threw an exception, terminate the sample, 
                    // and print the exception to the console.
                    // TODO (Left to the reader): Write real error handling code.
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine("Shutting down platform due to error: " + ex.Message);
                    //_helper.ShutdownPlatform();
                }
            }
        }


        private void SendMessageCompleted(IAsyncResult result)
        {
            InstantMessagingFlow instantMessagingFlow = result.AsyncState as InstantMessagingFlow;
            Exception ex = null;
            try
            {
                instantMessagingFlow.EndSendInstantMessage(result);
                Console.WriteLine("The message has been sent.");
            }
            catch (OperationTimeoutException opTimeEx)
            {
                // OperationFailureException: Indicates failure to connect the 
                // IM to the remote party due to timeout (called party failed to
                // respond within the expected time).
                // TODO (Left to the reader): Write real error handling code.
                ex = opTimeEx;
            }
            catch (RealTimeException rte)
            {
                // Other errors may cause other RealTimeExceptions to be thrown.
                // TODO (Left to the reader): Write real error handling code.
                ex = rte;
            }
            finally
            {
                // Reset the composing state of the local end user so that the typing notifcation as seen 
                // by the far end client disappears.
                instantMessagingFlow.LocalComposingState = ComposingState.Idle;
                if (ex != null)
                {
                    // If the action threw an exception, terminate the sample, 
                    // and print the exception to the console.
                    // TODO (Left to the reader): Write real error handling code.
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine("Shutting down platform due to error");
                    //_helper.ShutdownPlatform();
                }
            }
        }

        private void SendReply(InstantMessagingFlow instantMessagingFlow, Reply reply)
        {
            if (reply != null)
            {
                foreach (ReplyMessage replyMessage in reply.Messages)
                {
                    this.SendReply(instantMessagingFlow, replyMessage);
                }
            }
        }

        /// <summary>
        /// Sends the reply.
        /// </summary>
        /// <param name="instantMessagingFlow">The instant messaging flow.</param>
        /// <param name="replyMessage">The reply message.</param>
        private void SendReply(InstantMessagingFlow instantMessagingFlow, ReplyMessage replyMessage)
        {
            if (replyMessage != null && instantMessagingFlow.State != MediaFlowState.Terminated)
            {
                instantMessagingFlow.BeginSendInstantMessage(replyMessage.ContentType, replyMessage.Content.ToByteArray(), this.SendMessageCompleted, instantMessagingFlow);
            }
        }

        /// <summary>
        /// Setups a bot.
        /// </summary>
        /// <param name="instantMessagingFlow">The instant messaging flow.</param>
        /// <param name="conferenceUri">The conference URI.</param>
        private void RegisterBot(InstantMessagingFlow instantMessagingFlow, string conferenceUri)
        {
            this.RegisterBot(this.CreateBot(), instantMessagingFlow, conferenceUri);
        }

        /// <summary>
        /// Creates the bot.
        /// </summary>
        /// <returns></returns>
        public Bot CreateBot()
        {
            Bot bot = new Bot();
            bot.FeedbackEngine = this.FeedbackEngine;
            return bot;
        }
        /// <summary>
        /// Setups a bot.
        /// </summary>
        /// <param name="bot">The bot.</param>
        /// <param name="instantMessagingFlow">The instant messaging flow.</param>
        /// <param name="conferenceUri">The conference URI.</param>
        private void RegisterBot(Bot bot, InstantMessagingFlow instantMessagingFlow, string conferenceUri)
        {
            this.bots[instantMessagingFlow] = bot;

            // We need inline anonymous methods since we need to use the local variable instantMessagingFlow.
            this.bots[instantMessagingFlow].FailedToUnderstand +=
                  delegate(object messageHandlingCompletedSender, MessageEventArgs messageEventArgs)
                  {
                      // Need to send reply only if help text is specified. 
                      // If Bot is part of the conference, just need to notify only once.
                      if (!string.IsNullOrWhiteSpace(this.helpText))
                      {
                          bool needToRaiseRepliedEvent = false;
                          // In case Bot didn't understand what user said, it doesn't make sense to show same help message over and over again when users are part of the conference because most likely they are chatting to themselves.
                          // Thus, this will help with understanding when Bot need to show help message in case he didn't understand the user.
                          if (string.IsNullOrWhiteSpace(conferenceUri))
                          {
                              this.SendReply(instantMessagingFlow, new ReplyMessage(this.helpText, ReplyMessage.RtfTextContent));
                              needToRaiseRepliedEvent = true;
                          }
                          else if (this.conversationFlowMisunderstandingAlerts.ContainsKey(instantMessagingFlow) &&
                          !this.conversationFlowMisunderstandingAlerts[instantMessagingFlow])
                          {
                              this.conversationFlowMisunderstandingAlerts[instantMessagingFlow] = true;
                              this.SendReply(instantMessagingFlow, new ReplyMessage(this.helpText, ReplyMessage.RtfTextContent));
                              needToRaiseRepliedEvent = true;
                          }

                          if (needToRaiseRepliedEvent && this.Replied != null)
                          {
                              this.Replied(this, new ReplyEventArgs(new Reply(this.helpText), new Message(this.helpText), ReplyContext.RegularReplyMessage, 0, null));
                          }
                      }
                  };

            this.bots[instantMessagingFlow].Replied +=
                  delegate(object messageHandlingCompletedSender, ReplyEventArgs replyEventArgs)
                  {
                      this.SendReply(instantMessagingFlow, replyEventArgs.Reply);
                      if (this.Replied != null)
                      {
                          this.Replied(this, replyEventArgs);
                      }
                  };
        }
        /// <summary>
        /// Sends the reply.
        /// </summary>
        /// <param name="instantMessagingFlow">The instant messaging flow.</param>
        /// <param name="reply">The reply.</param>
       
      
        #endregion
    }
}
