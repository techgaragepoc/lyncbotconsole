﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class ACMESimulationMessageHandler : SingleStateMessageHandler
    {
        public ACMESimulationMessageHandler()
            : base(@"simulation|simulator|carry")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "simulation", "carry" };

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                var msgStartTags = "<html><head></head><body>";
                var msgEndTags = "</body></html>";
                var linebreak = "<br />";

                var simulator_url = "http://aumel13as26v/microstrategy/asp/Main.aspx?Server=-yrHDBTHJvj56J3FaGs_EEGDm3C4%3D&Project=Benefits+Forecaster&Port=-W9gxh6CAKd_WUbxs&evt=2048001&src=Main.aspx.2048001&documentID=7A81214849DD25E5DC7C0A906B2803B1&visMode=0";

                var replyMessage = msgStartTags + "yes, you can check <a href='" + simulator_url + "'>Benefit Forecaster</a> for further details" +
                                                 "" + msgEndTags;

                reply.AddHtmlMessage(replyMessage);
            }
           

            return reply;
        }
    }
}
