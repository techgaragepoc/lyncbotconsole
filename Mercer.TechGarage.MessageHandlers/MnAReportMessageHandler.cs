﻿using System;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;
//using Google.Console;
using System.Text;

namespace Mercer.TechGarage.MessageHandlers
{
    /// <summary>
    /// Help message handler.
    /// </summary>
    [Export(typeof(MessageHandler))]
    public class MnAReportMessageHandler : MessageHandler
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpMessageHandler"/> class.
        /// </summary>
        public MnAReportMessageHandler()
            : base("M&A")
        {
        }


        /// <summary>
        /// Gets the initial state handler.
        /// </summary>
        /// <value>The initial state handler.</value>
        protected override StateHandler InitialStateHandler
        {
            get { return AskForEmailReport; }
        }


        /// <summary>
        /// Asks for ticket status.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>


        public Reply AskForEmailReport(Message message)
        {
            Reply reply = new Reply();
            
            reply.Add("Here is a report Mercer published this year on Flight risk in M&A.");
            reply.AddRtfMessage(@"\b " + "2017 GLOBAL M&A RESEARCH REPORT, THE ART AND SCIENCE OF RETAINING TALENT" + @"\b0");
            //reply.AddHtmlMessage("<script> </script>");
            reply.AddHtmlMessage("<html><head><script> function hello(){alert('hello');} </script></head><body><p>Should I send a draft email to you with this report? (Yes/ No)<button onclick='hello()' value='Test'></button></p></body></html>");
            this.nextStateHandler = SendEmailStatus;
            return reply;
        }


        public Reply SendEmailStatus(Message message)
        {
            Reply reply = new Reply();
            if (message.Content.ToLower().Contains("yes"))
            {
                var outlookMail = new OutlookIntegration.OutlookMail();

                var mailSubject = "M&A Report for Review";
                var mailtext = "PFA M&A Report for your review before sending this to Susan";
                var mailRecipients = new String[] { "manish.kumar2@mercer.com", "arun.agrawal@mercer.com"  };
                outlookMail.SendMail(mailRecipients, mailSubject, mailtext);

                reply.Add("Great, an email has been sent to your inbox for review. :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            else
            {
                reply.Add("Ok. Thanks for confirmation :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            return reply;
        }



      
    }
}
