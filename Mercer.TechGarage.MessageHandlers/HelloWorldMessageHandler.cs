﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class HelloWorldMessageHandler : SingleStateMessageHandler
    {
        public HelloWorldMessageHandler()
            : base(@"hello|thanks|(how are you)")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            if (message.Content.ToLower().Equals("hello", StringComparison.InvariantCultureIgnoreCase) ||
                message.Content.ToLower().Equals("hi", StringComparison.InvariantCultureIgnoreCase))
            {
                reply.Add("hi there!");
            }
            else
            {
                if (message.Content.ToLower().Contains("thanks"))
                {
                    reply.Add(":)");
                }
                else
                {
                    // "how are you" case
                    reply.Add("I'm great, thanks!");
                }
            }

            return reply;
        }
    }
}
