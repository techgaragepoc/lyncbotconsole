﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class ACMEDetailsMessageHandler : SingleStateMessageHandler
    {
        public ACMEDetailsMessageHandler()
            : base(@"detail|details")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "detail", "details" };

            if (message.Content.ToLower().Contains("send"))
            {
                var outlookMail = new OutlookIntegration.OutlookMail();

                var mailSubject = "ACME - Offerings based on Benefits Forecaster";
                var mailtext = "PFA, both existing and suggested plan details for ACME";
                var mailTo = new String[] { message.SenderAlias + "@mercer.com" };
                
          
                outlookMail.SendMail(mailTo, mailSubject, mailtext);

                reply.Add("An email has been sent to your inbox for review :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            else {
                if (CommonMethods.IsMatched(possibleWords, message.Content))
                {
                    var msgStartTags = "<html><head></head><body>";
                    var msgEndTags = "</body></html>";
                    var linebreak = "<br />";

                    var replyMessage = msgStartTags + "ACME has around 750 employees covered for " +
                                            "Accident – AD&D, Disability – LTD, " +
                                            "Healthcare – Medical and Life – Term Life in the current plan " +
                                            "<b>Expatriate coverage for Canadian Employees</b> " +
                                            "with current premium <b>$1000, 044</b>" +
                                            "" + msgEndTags;

                    reply.AddHtmlMessage(replyMessage);
                }
            }

            return reply;
        }
    }
}
