﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mercer.TechGarage.MessageHandlers
{
   public static class CommonMethods
    {

        public static bool IsMatched(String[] possibleWords, string toMatch)
        {
            foreach (var eligibleWord in possibleWords)
            {
                if (toMatch.ToLower().Contains(eligibleWord.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
