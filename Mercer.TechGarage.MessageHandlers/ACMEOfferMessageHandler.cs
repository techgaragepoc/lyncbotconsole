﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class ACMEOfferMessageHandler : SingleStateMessageHandler
    {
        public ACMEOfferMessageHandler()
            : base(@"offer|reduce|premium reduction|down|bring it down")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "bring", "down","offer", "premium","reduction"};

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                var msgStartTags = "<html><head></head><body>";
                var msgEndTags = "</body></html>";
                var linebreak = "<br />";

                var replyMessage = msgStartTags + "Yes, here are my suggestions for ACME based on Benefits Forecaster - " +
                                            linebreak + "-	<b>Clinic Review</b>, offers chain of preferred Clinics that will avoid the inflated bills and eventually reduce their premium by ~10%, " +
                                            linebreak + "-	<b>Claims Audit</b>, a proactive audit will identify the discrepancies at early stage and pull down the premium cost by around 4%, " +
                                            linebreak + "-	<b>Wellness</b>, health awareness sessions and frequent checkups for employees will help to identify diseases at early stage and that can results reduction in premium by around 6%" +
                                            "" + msgEndTags;

                reply.AddHtmlMessage(replyMessage);
            }
           

            return reply;
        }
    }
}
