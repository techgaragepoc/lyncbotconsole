﻿using System;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;
//using Google.Console;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    public class WorkdayMessageHandler : MessageHandler
    {
        public WorkdayMessageHandler()
            : base("Workday|Pied Piper")
        {

        }

        protected override StateHandler InitialStateHandler
        {
            get { return updatequery; }
        }

        public Reply updatequery(Message message) {
            Reply reply = new Reply();
            reply.Add("You forecasted the Pied Piper consulting opportunity at $25,000 in MercerForce. We have a similar engagement with Hooli priced at $60,000. Do you want to udate the forecast in MercerForce? (Yes/ No)");
            this.nextStateHandler = checkUpdateStatus;
            return reply;
        }

        public Reply checkUpdateStatus(Message message) {
            Reply reply = new Reply();
            if (message.Content.Contains("Yes"))
            {
                reply.Add("Thanks, I've updated the forecast in MercerForce :)");
                reply.Add("Is there anything else i can help you with?");
                this.Done = true;
            }
            else{
                reply.Add("Thanks :)");
                reply.Add("Is there anything else i can help you with?");
                this.Done = true;
            }
            return reply;
        }
    }
}
