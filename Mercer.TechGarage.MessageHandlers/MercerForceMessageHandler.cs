﻿using System;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;
//using Google.Console;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    public class MercerForceMessageHandler : MessageHandler
    {

        public MercerForceMessageHandler()
            : base("MercerForce")
        {
        }

        protected override StateHandler InitialStateHandler
        {
            get { return UpdateForAcme; }
        }

        public Reply UpdateForAcme(Message message)
        {
            Reply reply = new Reply();
            reply.Add("I have updated MercerForce. Do you want to include a fee estimate? (Yes/ No)");
            this.nextStateHandler = checkEstimateStatus;
            return reply;
        }

        public Reply checkEstimateStatus(Message message)
        {
            Reply reply = new Reply();
            if (message.Content.Contains("Yes"))
            {
                reply.Add("Please tell me amount for which you want to send estimate.");
                this.nextStateHandler = getAmount;
                return reply;
            }
            else if (message.Content.Contains("No"))
            {
                reply.Add("Ok. Thanks.");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
                return reply;
            }
            else { 
                reply.Add("Ok. We have a similar engagement with Hooli priced at $125,000. Do you want to change the forecast in MercerForce? (Yes/ No)");
                this.nextStateHandler = changeEstimate;
                return reply;
            }

        }

        public Reply getAmount(Message message){
            Reply reply = new Reply();
            if (message.Content.Contains("$"))
            {
                reply.Add("Ok. We have a similar engagement with Hooli priced at $125,000. Do you want to change the forecast in MercerForce? (Yes/ No)");
                this.nextStateHandler = changeEstimate;
             }else{
                reply.Add("Invalid Amount!");
                
            }
            return reply;
        }

        public Reply changeEstimate(Message message){
            Reply reply = new Reply();
            if (message.Content.Contains("Yes"))
            {
                reply.Add("Thanks, I have updated the forecast in MercerForce.");
                reply.Add("Is there anything else i can help you with?");
                this.Done = true;
            }else{
                reply.Add("Thanks :)");
                reply.Add("Is there anything else i can help you with?");
                this.Done = true;
            }
            return reply;
        }

            
        
    }
}
