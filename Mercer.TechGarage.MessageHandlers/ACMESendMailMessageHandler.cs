﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class ACMESendMailMessageHandler : SingleStateMessageHandler
    {
        public ACMESendMailMessageHandler()
            : base(@"send|mail")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            if (message.Content.ToLower().Contains("send") || message.Content.ToLower().Contains("mail"))
            {
                var outlookMail = new OutlookIntegration.OutlookMail();

                var mailSubject = "ACME - Offerings based on Benefits Forecaster";
                var mailtext = "PFA, both existing and suggested plan details for ACME";
                var mailTo = new String[] { message.SenderAlias + "@mercer.com" };


                outlookMail.SendMail(mailTo, mailSubject, mailtext);

               /* var mailRecipients = new String[] {
                                                    // "manish.kumar2@mercer.com",
                                                    "pankaj.rautela@mercer.com",
                                                    "arun.agrawal@mercer.com" };
                outlookMail.SendMail(mailRecipients, mailSubject, mailtext);
                */

                reply.Add("An email has been sent to your inbox for review :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            else
            {
                reply.Add("Ok. Thanks for confirmation :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            return reply;

        }


    }
}
