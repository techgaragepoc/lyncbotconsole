﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class CoveragesMessageHandler : SingleStateMessageHandler
    {
        public CoveragesMessageHandler()
            : base(@"coverage|benefit")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "coverage", "coverages", "benefit", "benefits" };

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                reply.Add("You are covered for both Inpatient and Outpatient for all critical diseases");
            }
            else
            {
      
                reply.Add("what benefits/coverage you are looking for?");
            }

            return reply;
        }
    }
}
