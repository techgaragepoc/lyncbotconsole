﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class DoNotUnderstandMessageHandler : SingleStateMessageHandler
    {
        public DoNotUnderstandMessageHandler()
            : base(@"DNU")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();
            string helpText = string.Format("Sorry, I didn't get you. Type {0}  to see what I can understand by default.", "help");

           // if (message.Content.Equals("DNU", StringComparison.InvariantCultureIgnoreCase))
           // {
                reply.Add(helpText);
           // }
          //  else
          //  {
                // "how are you" case
           //     reply.Add("I'm great, thanks!");
           // }

            return reply;
        }
    }
}
