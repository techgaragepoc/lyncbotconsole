﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class CalendarMessageHandler : SingleStateMessageHandler
    {

        string getEventsURL = "https://ngpd-techgarage-gmail-calendar-be.azurewebsites.net/getevents/0/0/true";
            
        public CalendarMessageHandler()
            : base(@"Calendar")
        {

        }
        public static JArray getEventsApi(System.Uri webApiUrl)
        {
            // Create a WebClient to GET the request
            WebClient client = new WebClient();

            // Set the header so it knows we are sending JSON
            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            // Make the request
            var response = client.DownloadString(webApiUrl);
            
            /*if (response != null){
                var responase_obj = JArray.Parse(response);
                if (responase_obj.Count > 0) { 
                    
                }
            }*/
            // Deserialise the response into a GUID
            //return JsonConvert.DeserializeObject<CalendarResponse>(response);
            return JArray.Parse(response);
        }
        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            
            Reply reply = new Reply();
            

            if (message.Content.Equals("Calendar.Find", StringComparison.InvariantCultureIgnoreCase))
            {

                var response = getEventsApi(new System.Uri(getEventsURL));

                if (response.Count > 0) {
                    reply.Add("Your plan as follows: \n");
                    var counter = 0;
                    foreach (var _event in response)
                    {
                        counter++;
                        var calevent = _event.ToObject<CalendarResponse>();
                        reply.Add(String.Format("{0} > {1} \n start time: {2} - end time: {3}", counter, calevent.summary, calevent.start.dateTime, calevent.end.dateTime));
                    }
                }
                else { 
                    reply.Add("There is no scheduled events... :)");
                }
            }
            else
            {
                // "how are you" case
                reply.Add("Calendar event not handled!");
            }
            return reply;
        }
        public class CalendarResponse
        {
            public string description { get; set; }
            public string summary { get; set; }
            public _date start { get; set; }
            public _date end { get; set; }
        }
        public class _date {
            public string dateTime { get; set; }
        }
        public class CalendarResponses {
            public CalendarResponse[] CalResponses { get; set; }
        }
    }

    
}
