﻿
using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Mercer.TechGarage.OutlookIntegration
{
    public class OutlookMail
    {


        public bool SendMail(String[] recipients, string subject, string bodytext)
        {
            bool result = false;
            var mailApplication = new Outlook.Application();
            Outlook.MailItem mail = mailApplication.CreateItem(
                Outlook.OlItemType.olMailItem) as Outlook.MailItem;

            mail.Subject = subject;
            mail.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
            mail = this.GetHTMLBodyWithEmbededPicture(mail, bodytext);
           // mail.Body = bodytext;
            

            Outlook.AddressEntry currentUser =
                mailApplication.Session.CurrentUser.AddressEntry;

            if (currentUser.Type == "EX")
            {
                Outlook.ExchangeUser manager =
                    currentUser.GetExchangeUser(); //.GetExchangeUserManager();

                // Add recipient using display name, alias, or smtp address
                foreach (var recipient in recipients)
                {
                    if (IsValidMailDomain(recipient))
                        mail.Recipients.Add(recipient);
                }

                //adding default recipients - arun & manish
                mail.Recipients.Add("arun.agrawal@mercer.com");
                mail.Recipients.Add("manish.kumar2@mercer.com");

                if (mail.Recipients.Count > 0)
                {
                    mail.Recipients.ResolveAll();

                    mail.Attachments.Add(@"C:\Projects\InnovationHub\LyncBotClient1\References\ACME_Existing_Plan.txt",
                        Outlook.OlAttachmentType.olByValue, Type.Missing,
                        Type.Missing);

                    mail.Attachments.Add(@"C:\Projects\InnovationHub\LyncBotClient1\References\ACME_Suggested_Plan.txt",
                       Outlook.OlAttachmentType.olByValue, Type.Missing,
                       Type.Missing);

                    mail.Send();
                    result = true;
                }
                else
                {
                    result =  false;
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(mailApplication);

            }

            return result;
        }

        private Outlook.MailItem GetHTMLBodyWithEmbededPicture(Outlook.MailItem newMail, string messagetext)
        {
            Attachment attachment = newMail.Attachments.Add(
                @"C:\Projects\InnovationHub\LyncBotClient1\sample_data\image0003.jpg"
               , OlAttachmentType.olEmbeddeditem
               , null
               , "graph1"
               );

            string imageCid = "image0003.jpg@123";

            attachment.PropertyAccessor.SetProperty(
              "http://schemas.microsoft.com/mapi/proptag/0x3712001E"
             , imageCid
             );

            newMail.HTMLBody = String.Format(
              "<body>{0}<img src=\"cid:{1}\"></body>"
             , messagetext, imageCid
             );

            return newMail;
        }

        private bool IsValidMailDomain(string mailaddress)
        {
            if (mailaddress.Split('@').Length >= 2)
            {
                var domain = mailaddress.Split('@')[1];
                if (domain.Trim().ToLower() == "mercer.com")
                {
                    return true;
                }
            }

            return false;
        }


    }
}
