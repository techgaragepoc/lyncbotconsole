﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Mercer.TechGarage.OutlookIntegration
{
    public class OutlookCalendar
    {

        public void GetEvents()
        {
            Outlook.Application objOLApp;
            Outlook.MAPIFolder objFolder;
            Outlook.Explorer objExplorer;
            Outlook.MAPIFolder objSubFolder;
            Outlook.AppointmentItem objCalenderItem;
            Outlook.Folders objOutlookFolders;

            int intFolderCtr;
            int intSubFolderCtr;
            int intAppointmentCtr;

            // >> Initialize The Base Objects
            objOLApp = new Outlook.Application();
            objOutlookFolders = objOLApp.Session.Folders;
            // >> Loop Through The PST Files Added n Outlook
            for (intFolderCtr = 1; intFolderCtr <= objOutlookFolders.Count; intFolderCtr++)
            {
                objFolder = objOutlookFolders[intFolderCtr];
                objExplorer = objFolder.GetExplorer();
                // >> Loop Through The Folders In The PST File
                for (intSubFolderCtr = 1; intSubFolderCtr <= objExplorer.CurrentFolder.Folders.Count; intSubFolderCtr++)
                {
                    objSubFolder = objExplorer.CurrentFolder.Folders[intSubFolderCtr];
                    // >> Check if Folder Contains Appointment Items
                    if (objSubFolder.DefaultItemType == Outlook.OlItemType.olAppointmentItem)
                    {
                        // >> Loop Through Appointment Items
                        for (intAppointmentCtr = 1; intAppointmentCtr <= objSubFolder.Items.Count; intAppointmentCtr++)
                        {
   

                           if (objSubFolder.Items[intAppointmentCtr] is Outlook.AppointmentItem)
                            {
                           
                                // >> Get Teh Calender Item From The Calender Folder
                                objCalenderItem = objSubFolder.Items[intAppointmentCtr];

                                var lookFrom = DateTime.Now.AddHours(-2).ToUniversalTime();
                                var lookTill = DateTime.Now.AddDays(2).ToUniversalTime();

                                if (objCalenderItem.StartUTC >= lookFrom &&
                                        objCalenderItem.StartUTC < lookTill)
                                {
                                    // >> Process Appointment Item Accordingly
                                    
                                    Console.WriteLine(objCalenderItem.Organizer + ", " + 
                                                        objCalenderItem.Subject + ", " + 
                                                        objCalenderItem.Location);

                                   
                                }
                            }
                        }
                    }
                }
            }
            // >> Close Application
            objOLApp.Quit();
            // >> Release COM Object
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objOLApp);
            objOLApp = null;
            Console.ReadLine();
        }

    }
}
