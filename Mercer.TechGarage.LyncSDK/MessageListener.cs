﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Lync SDK references
using Microsoft.Lync.Utilities;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Conversation;
using Mercer.TechGarage.UCMA;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.LyncSDK
{
    public class MessageListener
    {
        private LyncClient _lyncClient;
        private ConversationManager _conversationManager;
        Bot bot = new Bot();

        public List<string> ParticipantList { get; set; }

        public MessageListener()
        {
            _lyncClient = LyncClient.GetClient();
            _conversationManager = _lyncClient.ConversationManager;
            _lyncClient.ConversationManager.ConversationAdded += ConversationAdded;

            ParticipantList = new List<string>();
        }



        private void ConversationAdded(object sender, ConversationManagerEventArgs e)
        {
            var conversation = e.Conversation;
            conversation.ParticipantAdded += ParticipantAdded;
        }

     

        private void ParticipantAdded(object sender, ParticipantCollectionChangedEventArgs e)
        {
            var participant = e.Participant;
            if (participant.IsSelf)
            {
                return;
            }

            
            participant.Conversation.StateChanged += conversation_StateChanged;

            var instantMessageModality =
                e.Participant.Modalities[ModalityTypes.InstantMessage] as InstantMessageModality;
            instantMessageModality.InstantMessageReceived += InstantMessageReceived;

            var participant_uri = participant.Contact.Uri.ToString().ToLower();
            if (ParticipantList.FindIndex(x => x.ToLower() == participant_uri) < 0)
            {
                ParticipantList.Add(participant_uri);
            }
        }

    

        private void InstantMessageReceived(object sender, MessageSentEventArgs e)
        {

            var text = e.Text.Replace(Environment.NewLine, string.Empty);
            Message message = new Message(text);

            bot.ProcessMessage(message, false);
            //var text = e.Text.Replace(Environment.NewLine, string.Empty);
            (sender as InstantMessageModality).BeginSendMessage(bot.Lync_reply, null, null);
        }

        private void conversation_StateChanged(object sender, ConversationStateChangedEventArgs e)
        {
            Conversation conversation = (Conversation)sender;


            if (conversation != null && (e.NewState == ConversationState.Inactive || e.NewState == ConversationState.Terminated))
            {
                try
                {
                    if (conversation.Participants.Count > 1)
                    {
                        var contactToRemove = conversation.Participants[1].Contact.Uri.ToString().ToLower();
                        if (ParticipantList.FindIndex(x => x.ToLower() == contactToRemove) >= 0)
                        {
                            ParticipantList.Remove(contactToRemove);
                        }
                        
                    }
                                        
                    conversation.End();
                }
                catch (Exception ex)
                {

                }
            }
        }


    }
}
