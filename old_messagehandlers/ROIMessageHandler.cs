﻿using System;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;
//using Google.Console;
using System.Text;

namespace Mercer.TechGarage.MessageHandlers
{
    /// <summary>
    /// Help message handler.
    /// </summary>
    [Export(typeof(MessageHandler))]
    public class ROIMessageHandler : MessageHandler
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ROIMessageHandler"/> class.
        /// </summary>
        public ROIMessageHandler()
            : base("ROI|per plan|ROI for clinic review")
        {
        }


        /// <summary>
        /// Gets the initial state handler.
        /// </summary>
        /// <value>The initial state handler.</value>
        protected override StateHandler InitialStateHandler
        {
            get { return AskForROIDetail; }
        }


        /// <summary>
        /// Asks for ticket status.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>


        public Reply AskForROIDetail(Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "ROI", "per plan", "ROI for clinic review" };

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                var msgStartTags = "<html><head></head><body>";
                var msgEndTags = "</body></html>";
                var linebreak = "<br />";

                var replyMessage = msgStartTags + "ACME employees usually pay $35 per visit and makes on average 5 visits " +
                                    "to special practitioner.  It falls in 75% percentile bucket for this industry " +
                                    "and region. If they opt for clinic review offering, they can avail 18:1 ROI easily " +
                                    "which accounts for a cummulative saving of $1,087,227 in 5 years. " +
                                    linebreak + linebreak + "Do you want me to send all these insights to you on as email? (Yes/ No)" + msgEndTags;

                reply.AddHtmlMessage(replyMessage);
                this.nextStateHandler = SendEmailStatus;
            }

                return reply;
        }


        public Reply SendEmailStatus(Message message)
        {
            Reply reply = new Reply();
            if (message.Content.ToLower().Contains("yes"))
            {
                var outlookMail = new OutlookIntegration.OutlookMail();

                var mailSubject = "ACME Offerings";
                var mailtext = "PFA, all three offerings for ACME";
                var mailRecipients = new String[] {
                    //"manish.kumar2@mercer.com",
                    "pankaj.rautela@mercer.com",
                    "arun.agrawal@mercer.com" };
                outlookMail.SendMail(mailRecipients, mailSubject, mailtext);

                reply.Add("Great, an email has been sent to your inbox for review. :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            else
            {
                reply.Add("Ok. Thanks for confirmation :)");
                reply.Add("Is there anything else I can help you with?");
                this.Done = true;
            }
            return reply;
        }



      
    }
}
