﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class SuggestionMessageHandler : SingleStateMessageHandler
    {
        public SuggestionMessageHandler()
            : base(@"suggestion|advice|advise")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "suggestion", "suggestion?", "advise","advise?","advice","advice?"};

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                var msg = "As their average loss ratio from last few renewal is around 80% which will " +
                                "trigger an increase in annual premium by 10-15%. This can be limit within 10% " +
                                "if their coverage of product B will be replaced with product E.";
                reply.Add(msg);
            }
            else
            {
                // "how are you" case
                reply.Add("I'm great, thanks!");
            }

            return reply;
        }


       
    }
}
