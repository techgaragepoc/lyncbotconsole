﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Mercer.TechGarage.UCMA.MessageHandlers;

namespace Mercer.TechGarage.MessageHandlers
{
    [Export(typeof(MessageHandler))]
    class OfferMessageHandler : SingleStateMessageHandler
    {
        public OfferMessageHandler()
            : base(@"offer")
        {
        }

        public override Reply Handle(Mercer.TechGarage.UCMA.MessageHandlers.Message message)
        {
            Reply reply = new Reply();

            var possibleWords = new String[] { "offer" };

            if (CommonMethods.IsMatched(possibleWords, message.Content))
            {
                var msgStartTags = "<html><head></head><body>";
                var msgEndTags = "</body></html>";
                var linebreak = "<br />";

                var replyMessage = msgStartTags + "I came up with 3 strategies to revise their current premium. " +
                                        linebreak + "1. <b>Clinic Review</b> – Benchmark your cost against peers & market, etc. " +
                                        linebreak + "2. <b>Claims Audit</b> - Identify procedural errors made by Insurers via claims audit. " +
                                        linebreak + "3. <b>Wellness</b> - Leverage health screening of employees for early risk detection, etc. " +
                                        linebreak + "Do you want to know ROI on these offerings?" 
                                        + msgEndTags;

                reply.AddHtmlMessage(replyMessage);
            }
           

            return reply;
        }
    }
}
