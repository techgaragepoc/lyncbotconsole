﻿namespace Mercer.TechGarage.BotManager
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.Dashboard_FormClosed();
            base.Dispose(disposing);
          
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.tabCtrlMain = new System.Windows.Forms.TabControl();
            this.tbPgMain = new System.Windows.Forms.TabPage();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtUri = new System.Windows.Forms.TextBox();
            this.lblURI = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.lblDomain = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblBotStatus = new System.Windows.Forms.Label();
            this.tbPgSendInstantMessage = new System.Windows.Forms.TabPage();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnCalEvents = new System.Windows.Forms.Button();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.btnInitiateMessage = new System.Windows.Forms.Button();
            this.cmbSendTo = new System.Windows.Forms.ComboBox();
            this.lblSendTo = new System.Windows.Forms.Label();
            this.txtboxMessage = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblSendHeader = new System.Windows.Forms.Label();
            this.tbPgReceiveMessage = new System.Windows.Forms.TabPage();
            this.lblReceiveHeader = new System.Windows.Forms.Label();
            this.lblMessageSentBy = new System.Windows.Forms.Label();
            this.lstbxMsgSentBy = new System.Windows.Forms.ListBox();
            this.tbPgSubscriber = new System.Windows.Forms.TabPage();
            this.lblSubsHelp = new System.Windows.Forms.Label();
            this.btnRemoveSubscriber = new System.Windows.Forms.Button();
            this.lstbxSubscribers = new System.Windows.Forms.ListBox();
            this.btnAddSubscriber = new System.Windows.Forms.Button();
            this.txtNewSubscriber = new System.Windows.Forms.TextBox();
            this.lblTechGarage = new System.Windows.Forms.Label();
            this.picBoxMercerLogo = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblBOTTitle = new System.Windows.Forms.Label();
            this.picBoxBOTImage = new System.Windows.Forms.PictureBox();
            this.tabCtrlMain.SuspendLayout();
            this.tbPgMain.SuspendLayout();
            this.tbPgSendInstantMessage.SuspendLayout();
            this.tbPgReceiveMessage.SuspendLayout();
            this.tbPgSubscriber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMercerLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxBOTImage)).BeginInit();
            this.SuspendLayout();
            // 
            // tabCtrlMain
            // 
            this.tabCtrlMain.Controls.Add(this.tbPgMain);
            this.tabCtrlMain.Controls.Add(this.tbPgSendInstantMessage);
            this.tabCtrlMain.Controls.Add(this.tbPgReceiveMessage);
            this.tabCtrlMain.Controls.Add(this.tbPgSubscriber);
            this.tabCtrlMain.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCtrlMain.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabCtrlMain.Location = new System.Drawing.Point(15, 100);
            this.tabCtrlMain.Name = "tabCtrlMain";
            this.tabCtrlMain.SelectedIndex = 0;
            this.tabCtrlMain.Size = new System.Drawing.Size(821, 383);
            this.tabCtrlMain.TabIndex = 0;
            this.tabCtrlMain.SelectedIndexChanged += new System.EventHandler(this.tabCtrlMain_SelectedIndexChanged);
            // 
            // tbPgMain
            // 
            this.tbPgMain.BackColor = System.Drawing.Color.LightGray;
            this.tbPgMain.Controls.Add(this.btnDisconnect);
            this.tbPgMain.Controls.Add(this.btnConnect);
            this.tbPgMain.Controls.Add(this.txtUri);
            this.tbPgMain.Controls.Add(this.lblURI);
            this.tbPgMain.Controls.Add(this.txtServer);
            this.tbPgMain.Controls.Add(this.lblServer);
            this.tbPgMain.Controls.Add(this.txtDomain);
            this.tbPgMain.Controls.Add(this.lblDomain);
            this.tbPgMain.Controls.Add(this.txtPassword);
            this.tbPgMain.Controls.Add(this.lblPassword);
            this.tbPgMain.Controls.Add(this.txtUserName);
            this.tbPgMain.Controls.Add(this.lblUserName);
            this.tbPgMain.Controls.Add(this.lblBotStatus);
            this.tbPgMain.Location = new System.Drawing.Point(4, 26);
            this.tbPgMain.Name = "tbPgMain";
            this.tbPgMain.Padding = new System.Windows.Forms.Padding(3);
            this.tbPgMain.Size = new System.Drawing.Size(813, 353);
            this.tbPgMain.TabIndex = 2;
            this.tbPgMain.Text = "BOT Detail";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.BackColor = System.Drawing.Color.Firebrick;
            this.btnDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDisconnect.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisconnect.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDisconnect.Location = new System.Drawing.Point(391, 292);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(127, 33);
            this.btnDisconnect.TabIndex = 20;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = false;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.Color.ForestGreen;
            this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnConnect.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnConnect.Location = new System.Drawing.Point(258, 292);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(127, 33);
            this.btnConnect.TabIndex = 19;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = false;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txtUri
            // 
            this.txtUri.Location = new System.Drawing.Point(334, 216);
            this.txtUri.Name = "txtUri";
            this.txtUri.Size = new System.Drawing.Size(233, 24);
            this.txtUri.TabIndex = 18;
            // 
            // lblURI
            // 
            this.lblURI.AutoSize = true;
            this.lblURI.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURI.Location = new System.Drawing.Point(212, 220);
            this.lblURI.Name = "lblURI";
            this.lblURI.Size = new System.Drawing.Size(34, 18);
            this.lblURI.TabIndex = 17;
            this.lblURI.Text = "URI";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(334, 181);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(233, 24);
            this.txtServer.TabIndex = 16;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServer.Location = new System.Drawing.Point(212, 185);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(57, 18);
            this.lblServer.TabIndex = 15;
            this.lblServer.Text = "Server";
            // 
            // txtDomain
            // 
            this.txtDomain.Location = new System.Drawing.Point(334, 147);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.Size = new System.Drawing.Size(233, 24);
            this.txtDomain.TabIndex = 14;
            // 
            // lblDomain
            // 
            this.lblDomain.AutoSize = true;
            this.lblDomain.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDomain.Location = new System.Drawing.Point(212, 151);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.Size = new System.Drawing.Size(61, 18);
            this.lblDomain.TabIndex = 13;
            this.lblDomain.Text = "Domain";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(334, 114);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(233, 24);
            this.txtPassword.TabIndex = 12;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(212, 118);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(77, 18);
            this.lblPassword.TabIndex = 11;
            this.lblPassword.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(334, 78);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(233, 24);
            this.txtUserName.TabIndex = 10;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(212, 82);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(86, 18);
            this.lblUserName.TabIndex = 9;
            this.lblUserName.Text = "User Name";
            // 
            // lblBotStatus
            // 
            this.lblBotStatus.AutoSize = true;
            this.lblBotStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBotStatus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblBotStatus.Location = new System.Drawing.Point(639, 27);
            this.lblBotStatus.Name = "lblBotStatus";
            this.lblBotStatus.Size = new System.Drawing.Size(129, 20);
            this.lblBotStatus.TabIndex = 7;
            this.lblBotStatus.Text = "Disconnnected";
            // 
            // tbPgSendInstantMessage
            // 
            this.tbPgSendInstantMessage.BackColor = System.Drawing.Color.LightGray;
            this.tbPgSendInstantMessage.Controls.Add(this.txtTitle);
            this.tbPgSendInstantMessage.Controls.Add(this.lblTitle);
            this.tbPgSendInstantMessage.Controls.Add(this.btnCalEvents);
            this.tbPgSendInstantMessage.Controls.Add(this.btnSendMail);
            this.tbPgSendInstantMessage.Controls.Add(this.btnInitiateMessage);
            this.tbPgSendInstantMessage.Controls.Add(this.cmbSendTo);
            this.tbPgSendInstantMessage.Controls.Add(this.lblSendTo);
            this.tbPgSendInstantMessage.Controls.Add(this.txtboxMessage);
            this.tbPgSendInstantMessage.Controls.Add(this.lblMessage);
            this.tbPgSendInstantMessage.Controls.Add(this.lblSendHeader);
            this.tbPgSendInstantMessage.Location = new System.Drawing.Point(4, 26);
            this.tbPgSendInstantMessage.Name = "tbPgSendInstantMessage";
            this.tbPgSendInstantMessage.Padding = new System.Windows.Forms.Padding(3);
            this.tbPgSendInstantMessage.Size = new System.Drawing.Size(813, 353);
            this.tbPgSendInstantMessage.TabIndex = 0;
            this.tbPgSendInstantMessage.Text = "Send Instant Message";
            // 
            // txtTitle
            // 
            this.txtTitle.Font = new System.Drawing.Font("Segoe Print", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.Location = new System.Drawing.Point(107, 55);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(630, 33);
            this.txtTitle.TabIndex = 9;
            this.txtTitle.Text = "Notification from BOT";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(20, 55);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 18);
            this.lblTitle.TabIndex = 8;
            this.lblTitle.Text = "Title";
            // 
            // btnCalEvents
            // 
            this.btnCalEvents.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnCalEvents.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCalEvents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalEvents.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnCalEvents.Location = new System.Drawing.Point(637, 291);
            this.btnCalEvents.Name = "btnCalEvents";
            this.btnCalEvents.Size = new System.Drawing.Size(147, 31);
            this.btnCalEvents.TabIndex = 7;
            this.btnCalEvents.Text = "Get Cal Events";
            this.btnCalEvents.UseVisualStyleBackColor = false;
            this.btnCalEvents.Visible = false;
            this.btnCalEvents.Click += new System.EventHandler(this.btnCalEvents_Click);
            // 
            // btnSendMail
            // 
            this.btnSendMail.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSendMail.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSendMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendMail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnSendMail.Location = new System.Drawing.Point(504, 291);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(127, 31);
            this.btnSendMail.TabIndex = 6;
            this.btnSendMail.Text = "Send Mail";
            this.btnSendMail.UseVisualStyleBackColor = false;
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // btnInitiateMessage
            // 
            this.btnInitiateMessage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnInitiateMessage.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInitiateMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInitiateMessage.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInitiateMessage.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnInitiateMessage.Location = new System.Drawing.Point(107, 291);
            this.btnInitiateMessage.Name = "btnInitiateMessage";
            this.btnInitiateMessage.Size = new System.Drawing.Size(127, 31);
            this.btnInitiateMessage.TabIndex = 5;
            this.btnInitiateMessage.Text = "Initiate";
            this.btnInitiateMessage.UseVisualStyleBackColor = false;
            this.btnInitiateMessage.Click += new System.EventHandler(this.btnInitiateMessage_Click);
            // 
            // cmbSendTo
            // 
            this.cmbSendTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSendTo.FormattingEnabled = true;
            this.cmbSendTo.Location = new System.Drawing.Point(107, 232);
            this.cmbSendTo.Name = "cmbSendTo";
            this.cmbSendTo.Size = new System.Drawing.Size(285, 25);
            this.cmbSendTo.TabIndex = 4;
            // 
            // lblSendTo
            // 
            this.lblSendTo.AutoSize = true;
            this.lblSendTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSendTo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblSendTo.Location = new System.Drawing.Point(22, 235);
            this.lblSendTo.Name = "lblSendTo";
            this.lblSendTo.Size = new System.Drawing.Size(71, 18);
            this.lblSendTo.TabIndex = 3;
            this.lblSendTo.Text = "Send To";
            // 
            // txtboxMessage
            // 
            this.txtboxMessage.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtboxMessage.Location = new System.Drawing.Point(107, 94);
            this.txtboxMessage.Multiline = true;
            this.txtboxMessage.Name = "txtboxMessage";
            this.txtboxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtboxMessage.Size = new System.Drawing.Size(630, 109);
            this.txtboxMessage.TabIndex = 2;
            this.txtboxMessage.Text = "Hi, This is a BOT.";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMessage.Location = new System.Drawing.Point(19, 94);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(76, 18);
            this.lblMessage.TabIndex = 1;
            this.lblMessage.Text = "Message";
            // 
            // lblSendHeader
            // 
            this.lblSendHeader.AutoSize = true;
            this.lblSendHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSendHeader.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblSendHeader.Location = new System.Drawing.Point(16, 16);
            this.lblSendHeader.Name = "lblSendHeader";
            this.lblSendHeader.Size = new System.Drawing.Size(338, 20);
            this.lblSendHeader.TabIndex = 0;
            this.lblSendHeader.Text = "Use this interface to initiate a message...";
            // 
            // tbPgReceiveMessage
            // 
            this.tbPgReceiveMessage.BackColor = System.Drawing.Color.LightGray;
            this.tbPgReceiveMessage.Controls.Add(this.lblReceiveHeader);
            this.tbPgReceiveMessage.Controls.Add(this.lblMessageSentBy);
            this.tbPgReceiveMessage.Controls.Add(this.lstbxMsgSentBy);
            this.tbPgReceiveMessage.Location = new System.Drawing.Point(4, 26);
            this.tbPgReceiveMessage.Name = "tbPgReceiveMessage";
            this.tbPgReceiveMessage.Padding = new System.Windows.Forms.Padding(3);
            this.tbPgReceiveMessage.Size = new System.Drawing.Size(813, 353);
            this.tbPgReceiveMessage.TabIndex = 1;
            this.tbPgReceiveMessage.Text = "Receive Message(s)";
            // 
            // lblReceiveHeader
            // 
            this.lblReceiveHeader.AutoSize = true;
            this.lblReceiveHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveHeader.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblReceiveHeader.Location = new System.Drawing.Point(24, 18);
            this.lblReceiveHeader.Name = "lblReceiveHeader";
            this.lblReceiveHeader.Size = new System.Drawing.Size(357, 20);
            this.lblReceiveHeader.TabIndex = 2;
            this.lblReceiveHeader.Text = "It will show the list of user chating with BOT";
            // 
            // lblMessageSentBy
            // 
            this.lblMessageSentBy.AutoSize = true;
            this.lblMessageSentBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageSentBy.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMessageSentBy.Location = new System.Drawing.Point(25, 97);
            this.lblMessageSentBy.Name = "lblMessageSentBy";
            this.lblMessageSentBy.Size = new System.Drawing.Size(201, 18);
            this.lblMessageSentBy.TabIndex = 1;
            this.lblMessageSentBy.Text = "Message Sent By (Active)";
            // 
            // lstbxMsgSentBy
            // 
            this.lstbxMsgSentBy.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstbxMsgSentBy.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lstbxMsgSentBy.FormattingEnabled = true;
            this.lstbxMsgSentBy.ItemHeight = 26;
            this.lstbxMsgSentBy.Location = new System.Drawing.Point(245, 86);
            this.lstbxMsgSentBy.Name = "lstbxMsgSentBy";
            this.lstbxMsgSentBy.Size = new System.Drawing.Size(366, 238);
            this.lstbxMsgSentBy.TabIndex = 0;
            // 
            // tbPgSubscriber
            // 
            this.tbPgSubscriber.BackColor = System.Drawing.Color.LightGray;
            this.tbPgSubscriber.Controls.Add(this.lblSubsHelp);
            this.tbPgSubscriber.Controls.Add(this.btnRemoveSubscriber);
            this.tbPgSubscriber.Controls.Add(this.lstbxSubscribers);
            this.tbPgSubscriber.Controls.Add(this.btnAddSubscriber);
            this.tbPgSubscriber.Controls.Add(this.txtNewSubscriber);
            this.tbPgSubscriber.Location = new System.Drawing.Point(4, 26);
            this.tbPgSubscriber.Name = "tbPgSubscriber";
            this.tbPgSubscriber.Size = new System.Drawing.Size(813, 353);
            this.tbPgSubscriber.TabIndex = 3;
            this.tbPgSubscriber.Text = "Subscriber(s)";
            // 
            // lblSubsHelp
            // 
            this.lblSubsHelp.AutoSize = true;
            this.lblSubsHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubsHelp.ForeColor = System.Drawing.SystemColors.Desktop;
            this.lblSubsHelp.Location = new System.Drawing.Point(23, 318);
            this.lblSubsHelp.Name = "lblSubsHelp";
            this.lblSubsHelp.Size = new System.Drawing.Size(500, 20);
            this.lblSubsHelp.TabIndex = 4;
            this.lblSubsHelp.Text = "* To apply the changes, please disconnect and connect again";
            // 
            // btnRemoveSubscriber
            // 
            this.btnRemoveSubscriber.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnRemoveSubscriber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveSubscriber.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnRemoveSubscriber.Location = new System.Drawing.Point(518, 250);
            this.btnRemoveSubscriber.Name = "btnRemoveSubscriber";
            this.btnRemoveSubscriber.Size = new System.Drawing.Size(175, 30);
            this.btnRemoveSubscriber.TabIndex = 3;
            this.btnRemoveSubscriber.Text = "Remove Subscriber";
            this.btnRemoveSubscriber.UseVisualStyleBackColor = false;
            this.btnRemoveSubscriber.Click += new System.EventHandler(this.btnRemoveSubscriber_Click);
            // 
            // lstbxSubscribers
            // 
            this.lstbxSubscribers.FormattingEnabled = true;
            this.lstbxSubscribers.ItemHeight = 17;
            this.lstbxSubscribers.Location = new System.Drawing.Point(127, 106);
            this.lstbxSubscribers.Name = "lstbxSubscribers";
            this.lstbxSubscribers.Size = new System.Drawing.Size(357, 174);
            this.lstbxSubscribers.TabIndex = 2;
            // 
            // btnAddSubscriber
            // 
            this.btnAddSubscriber.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnAddSubscriber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddSubscriber.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnAddSubscriber.Location = new System.Drawing.Point(518, 42);
            this.btnAddSubscriber.Name = "btnAddSubscriber";
            this.btnAddSubscriber.Size = new System.Drawing.Size(175, 30);
            this.btnAddSubscriber.TabIndex = 1;
            this.btnAddSubscriber.Text = "Add Subscriber";
            this.btnAddSubscriber.UseVisualStyleBackColor = false;
            this.btnAddSubscriber.Click += new System.EventHandler(this.btnAddSubscriber_Click);
            // 
            // txtNewSubscriber
            // 
            this.txtNewSubscriber.Location = new System.Drawing.Point(127, 45);
            this.txtNewSubscriber.Name = "txtNewSubscriber";
            this.txtNewSubscriber.Size = new System.Drawing.Size(357, 24);
            this.txtNewSubscriber.TabIndex = 0;
            // 
            // lblTechGarage
            // 
            this.lblTechGarage.AutoSize = true;
            this.lblTechGarage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTechGarage.ForeColor = System.Drawing.Color.Lavender;
            this.lblTechGarage.Location = new System.Drawing.Point(628, 502);
            this.lblTechGarage.Name = "lblTechGarage";
            this.lblTechGarage.Size = new System.Drawing.Size(202, 25);
            this.lblTechGarage.TabIndex = 1;
            this.lblTechGarage.Text = "Technology Garage";
            // 
            // picBoxMercerLogo
            // 
            this.picBoxMercerLogo.BackColor = System.Drawing.Color.DimGray;
            this.picBoxMercerLogo.Image = ((System.Drawing.Image)(resources.GetObject("picBoxMercerLogo.Image")));
            this.picBoxMercerLogo.Location = new System.Drawing.Point(17, 505);
            this.picBoxMercerLogo.Name = "picBoxMercerLogo";
            this.picBoxMercerLogo.Size = new System.Drawing.Size(167, 36);
            this.picBoxMercerLogo.TabIndex = 2;
            this.picBoxMercerLogo.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnClose.Location = new System.Drawing.Point(730, 20);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(104, 37);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblBOTTitle
            // 
            this.lblBOTTitle.AutoSize = true;
            this.lblBOTTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblBOTTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBOTTitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblBOTTitle.Location = new System.Drawing.Point(76, 24);
            this.lblBOTTitle.Name = "lblBOTTitle";
            this.lblBOTTitle.Size = new System.Drawing.Size(391, 31);
            this.lblBOTTitle.TabIndex = 5;
            this.lblBOTTitle.Text = "BOT Console for Skype/Lync";
            // 
            // picBoxBOTImage
            // 
            this.picBoxBOTImage.BackColor = System.Drawing.Color.Transparent;
            this.picBoxBOTImage.Image = ((System.Drawing.Image)(resources.GetObject("picBoxBOTImage.Image")));
            this.picBoxBOTImage.Location = new System.Drawing.Point(13, 20);
            this.picBoxBOTImage.Name = "picBoxBOTImage";
            this.picBoxBOTImage.Size = new System.Drawing.Size(48, 40);
            this.picBoxBOTImage.TabIndex = 6;
            this.picBoxBOTImage.TabStop = false;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(865, 554);
            this.Controls.Add(this.picBoxBOTImage);
            this.Controls.Add(this.lblBOTTitle);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.picBoxMercerLogo);
            this.Controls.Add(this.lblTechGarage);
            this.Controls.Add(this.tabCtrlMain);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.tabCtrlMain.ResumeLayout(false);
            this.tbPgMain.ResumeLayout(false);
            this.tbPgMain.PerformLayout();
            this.tbPgSendInstantMessage.ResumeLayout(false);
            this.tbPgSendInstantMessage.PerformLayout();
            this.tbPgReceiveMessage.ResumeLayout(false);
            this.tbPgReceiveMessage.PerformLayout();
            this.tbPgSubscriber.ResumeLayout(false);
            this.tbPgSubscriber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMercerLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxBOTImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrlMain;
        private System.Windows.Forms.TabPage tbPgMain;
        private System.Windows.Forms.TabPage tbPgSendInstantMessage;
        private System.Windows.Forms.TabPage tbPgReceiveMessage;
        private System.Windows.Forms.TabPage tbPgSubscriber;
        private System.Windows.Forms.Label lblSendTo;
        private System.Windows.Forms.TextBox txtboxMessage;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblSendHeader;
        private System.Windows.Forms.Button btnInitiateMessage;
        private System.Windows.Forms.ComboBox cmbSendTo;

        private System.Windows.Forms.Label lblBotStatus;
        private System.Windows.Forms.Label lblTechGarage;
        private System.Windows.Forms.Label lblMessageSentBy;
        private System.Windows.Forms.ListBox lstbxMsgSentBy;
        private System.Windows.Forms.PictureBox picBoxMercerLogo;
        private System.Windows.Forms.Button btnSendMail;
        private System.Windows.Forms.Button btnCalEvents;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRemoveSubscriber;
        private System.Windows.Forms.ListBox lstbxSubscribers;
        private System.Windows.Forms.Button btnAddSubscriber;
        private System.Windows.Forms.TextBox txtNewSubscriber;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtUri;
        private System.Windows.Forms.Label lblURI;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.Label lblDomain;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblBOTTitle;
        private System.Windows.Forms.Label lblReceiveHeader;
        private System.Windows.Forms.Label lblSubsHelp;
        private System.Windows.Forms.PictureBox picBoxBOTImage;
    }
}

