﻿using Mercer.TechGarage.LyncSDK;
using Mercer.TechGarage.UCMA;
using Microsoft.Rtc.Collaboration.Sample.BasicInstantMessagingCall;
using Microsoft.Rtc.Collaboration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Rtc.Collaboration.Sample.Common;

namespace Mercer.TechGarage.BotManager
{
    public partial class Dashboard : Form
    {

        Thread _monitorParticipant;

        //private ApplicationEndpoint _userEndpoint;
        private UserEndpoint _userEndpoint;
        private UCMASampleHelper _helper;

        private InstantMessagingCallNotifier _InstantMessagingCallNotifier = new InstantMessagingCallNotifier();
        private InstantMessagingCallListener _InstantMessagingCallListener = new InstantMessagingCallListener();

        private string initiateMessage = "You have a meeting with Susan Burke at ACME in next 2 hours to discuss the policy renewal.";
        private string messagetitle = "ACME Meeting";

        public Dashboard()
        {
            InitializeComponent();
                
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
  
            //fill the default subscribers
            lstbxSubscribers.Items.Clear();
            foreach (var subscriber in BotConfig.Subscribers)
            {
                lstbxSubscribers.Items.Add(subscriber);
                if (_InstantMessagingCallListener != null)
                {
                    _InstantMessagingCallListener.Subscribers.Add(subscriber);
                }
            }

            //fill the connection information
            txtServer.Text = BotConfig.Server;
            txtDomain.Text = BotConfig.Domain;
            txtUserName.Text = BotConfig.UserName;
            txtPassword.Text = BotConfig.Password;
            txtUri.Text = BotConfig.UserURI;

            //initially Bot is not connected
            SetConnectedStatus(false);

            this.Show();
        }

       

        private void Dashboard_FormClosed()
        {
            ShutdownIM();
            Application.Exit();
        }


        private void populate_SendTo()
        {
            cmbSendTo.Items.Clear();

            foreach(var subscriber in lstbxSubscribers.Items)
            {
                cmbSendTo.Items.Add(subscriber);
            }

            if (cmbSendTo.Items.Count > 0)
            {
                cmbSendTo.SelectedIndex = 0;
            }
        }


        private void btnInitiateMessage_Click(object sender, EventArgs e)
        {
            if (_userEndpoint != null && !String.IsNullOrEmpty(cmbSendTo.SelectedItem.ToString().Trim()))
            {
                var sipOfsendTo = "sip:" + cmbSendTo.SelectedItem.ToString().Trim();

                var monitorNewMessage = new Thread(() => NewMessageThread(_userEndpoint, sipOfsendTo, txtTitle.Text.Trim(), txtboxMessage.Text.Trim()));
                monitorNewMessage.Start();
            }
        }
                
        public void NewMessageThread(UserEndpoint userEndpoint ,string sipOfsendTo, string messagetitle,string message)
        {
            _InstantMessagingCallNotifier.Run(userEndpoint,sipOfsendTo, messagetitle, message);
        }

        private void checkParticipants (int intervalInSec)
        {
            try
            {
                
                while (true)
                {
                    this.Invoke((MethodInvoker)(() =>
                    {

                        lstbxMsgSentBy.Items.Clear();

                        if (_InstantMessagingCallListener != null && 
                                _InstantMessagingCallListener.MessageSenders != null &&
                                _InstantMessagingCallListener.MessageSenders.Count > 0)
                        {

                            foreach (var participant in _InstantMessagingCallListener.MessageSenders)
                            {
                                lstbxMsgSentBy.Items.Add(participant.Replace("sip:",""));
                            }
                        }

                    }));


                    Thread.Sleep(intervalInSec * 1000); // in every 3 seconds check the list of participants
                }
            }
            catch(Exception ex)
            {

            }

        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            var OutlookMail = new OutlookIntegration.OutlookMail();

            var mailSubject = "test mail";
            var mailtext = "This is a test mail from BOT code";
            var mailRecipients = new String[] { "arun.agrawal@mercer.com" };
            OutlookMail.SendMail(mailRecipients, mailSubject, mailtext);

            
        }

        private void btnCalEvents_Click(object sender, EventArgs e)
        {
            var OutlookCalendar = new OutlookIntegration.OutlookCalendar();
            OutlookCalendar.GetEvents();

        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        

        private void tabCtrlMain_SelectedIndexChanged(Object sender, EventArgs e)
        {
            var selectedTabPage = ((TabControl)sender).SelectedTab;

            if(selectedTabPage.Name == this.tbPgMain.Name)
            {

            }
           
           if (selectedTabPage.Name == this.tbPgSendInstantMessage.Name)
            {
                txtTitle.Text = (String.IsNullOrEmpty(txtTitle.Text.Trim()))?messagetitle: txtTitle.Text;
                txtboxMessage.Text = (String.IsNullOrEmpty(txtboxMessage.Text.Trim())) ? initiateMessage: txtboxMessage.Text;

                populate_SendTo();
            }

           if (selectedTabPage.Name == this.tbPgReceiveMessage.Name)
            {

            }

           if (selectedTabPage.Name == this.tbPgSubscriber.Name)
            {

            }

        }


        public bool ConnectIM()
        {
            try
            {
                // Create the UserEndpoint
                _helper = new UCMASampleHelper();

                _userEndpoint = _helper.CreateEstablishedUserEndpoint("BOT Messaging", txtServer.Text,
                                                txtDomain.Text, txtUserName.Text, txtPassword.Text, txtUri.Text);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void ShutdownIM()
        {
            if (_helper != null)
            {
                _helper.ShutdownPlatform();
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            var loadingscreen = new LoadingScreen();
            loadingscreen.StartPosition = FormStartPosition.CenterParent;
            loadingscreen.Show();

            this.Enabled = false;

            if (!ConnectIM())
            {
                MessageBox.Show("Error occured while connecting IM...");
            }
            else
            {
               _InstantMessagingCallListener.Initialize(_userEndpoint);

                //start thread to check senders at every 3 second
                _monitorParticipant = new Thread(() => checkParticipants(3));
                _monitorParticipant.Start();

                SetConnectedStatus(true);
            }

            loadingscreen.Close();
            this.Enabled = true;
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {

            if (_monitorParticipant != null)
            {
                try
                {
                    _monitorParticipant.Abort();
                    _monitorParticipant = null;

                    ShutdownIM();
                }
                catch(Exception ex)
                {

                }
            }
            SetConnectedStatus(false);

        }

        private void SetConnectedStatus(bool connectedStatus)
        {
            if (connectedStatus)
            {
                btnConnect.Enabled = false;
                btnDisconnect.Enabled = true;
                lblBotStatus.Text = "Connected";
                lblBotStatus.ForeColor = Color.Green;
            }
            else
            {
                btnConnect.Enabled = true;
                btnDisconnect.Enabled = false;
                lblBotStatus.Text = "Disconnected";
                lblBotStatus.ForeColor = Color.Red;
            }
        }

        private void btnAddSubscriber_Click(object sender, EventArgs e)
        {
            var isAlreadyExist = false;
            var newSubscriber = (!String.IsNullOrEmpty(txtNewSubscriber.Text))?txtNewSubscriber.Text.Trim().ToLower():String.Empty;

            if (lstbxSubscribers.Items.Count>0)
            {
                foreach(var subs in lstbxSubscribers.Items)
                {
                    if (subs.ToString() == newSubscriber)
                    {
                        isAlreadyExist = true;
                        break;
                    }
                }
            }

            if (!isAlreadyExist)
            {
                lstbxSubscribers.Items.Add(newSubscriber);
                if(_InstantMessagingCallListener != null)
                {
                    _InstantMessagingCallListener.Subscribers.Add(newSubscriber);
                }
            }
        }

        private void btnRemoveSubscriber_Click(object sender, EventArgs e)
        {
            if (lstbxSubscribers.Items.Count > 0 && lstbxSubscribers.SelectedIndex >= 0)
            {
                var subsToRemove = lstbxSubscribers.Items[lstbxSubscribers.SelectedIndex];
                lstbxSubscribers.Items.RemoveAt(lstbxSubscribers.SelectedIndex);
                if (_InstantMessagingCallListener != null)
                {
                    _InstantMessagingCallListener.Subscribers.Remove(subsToRemove.ToString());
                }

            }
        }
        

    }
}
