﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mercer.TechGarage.BotManager
{
    public static class BotConfig
    {

        public static string UserName { get; set; }

        public static string Password { get; set; }

        public static string Domain { get; set; }

        public static string Server { get; set; }

        public static string UserURI { get; set; }

        public static List<string> Subscribers { get; set; }

        static BotConfig()
        {

           Server = GetConfigValue("ServerFQDN");
           Domain = GetConfigValue("domain");
           UserName = GetConfigValue("user_name");
           Password = GetConfigValue("password");
           UserURI = GetConfigValue("UserURI");

           Subscribers = new List<string>();

           var subscriberlist = GetConfigValue("default_subscribers");

           if (!String.IsNullOrEmpty(subscriberlist))
            {
                var subs = subscriberlist.Split(',');
                foreach(var sub in subs)
                {
                    Subscribers.Add(sub);
                }
            }

        }

        private static string GetConfigValue(string key)
        {
               return (!String.IsNullOrEmpty(ConfigurationManager.AppSettings[key]))?ConfigurationManager.AppSettings[key]:String.Empty;
        }

    }
}
